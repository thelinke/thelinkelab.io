---
title: Self-introduction
date: 2019-06-29 01:29:30
categories: 碎碎念
tags: 
- 面试
- 保研分享
---
Good morning/afternoon, my dear professors:
　　First of all, please allow me to express my sincere appreciation for the opportunity that you give me to join the interview.
My name is Lin Ke, comes from Hefei University of Technology. I major in Electrical Engineering and Its Automation and I am interested in Power Electronics and Power Drives.<!-- more -->
　　In the past three years, I have been working very hard. I ranked No.8 in my grade with two hundred and ninety-four students in total. And I really hope to be excellent here as how I was before.The first reason I come here is that the academic atmosphere in Southern University of Science and Technology deeply appeals to me. Secondly, Shenzhen is a highly-developed city. Here offers me a gold opportunity to experience new things and broaden my horizon.
　　Last summer, I took part in the Twelfth International Contest of innovation as core member. Our team designed a mechanical arm with image recognition which helps waste recycling. In this competition, I mainly manage to control the mechanical arm by MCU Programming. I also set up web server and develop a wechat app for our project. Eventually, We obtained the second prize in Anhui Division.
　　As for my personality, I am quick in thought, creative and easy-going. I like programming and basketball. I dream to be an Electrical Engineer and hope to achieve my goal here.